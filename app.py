from fastapi import FastAPI
from typing import Optional, List
from pydantic import BaseModel
from fastapi.responses import FileResponse
from Models.Item import Item
from uuid import uuid4, UUID

app = FastAPI()

fakeDB: List[Item] = []


# App Routes
@app.get("/", response_class=FileResponse)
def home():
    return FileResponse("./public/index.html")


# Get all items
@app.get("/items", response_model=List[Item])
def get_items():
    return fakeDB


# Get a item
@app.get("/items/{item_id}", response_model=Item)
def get_a_item(item_id: UUID):
    return next(item for item in fakeDB if item.id == item_id)


# Create a new item
@app.post("/items", response_model=Item)
def create_item(item: Item):
    item.id = uuid4()
    fakeDB.append(item)
    return item