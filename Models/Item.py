from pydantic import BaseModel 
from typing import Optional
from uuid import UUID

class Item(BaseModel):
    id: Optional[UUID]
    item: str

    